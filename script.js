// 1. Створіть об'єкт product з властивостями name, price та discount. Додайте метод для виведення повної ціни товару з урахуванням знижки. Викличте цей метод та результат виведіть в консоль.

let product = {
    name: "Телефон",
    price: 1000,
    discount: 10,

    getTotalPrice: function() {
        let discountedPrice = this.price * (100 - this.discount) / 100;
        return discountedPrice;
    }
};

let clonedProduct = cloneObject(product);

console.log("Повна ціна товару з урахуванням знижки:", clonedProduct.getTotalPrice());

// 2. Напишіть функцію greeting, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком,

// наприклад "Привіт, мені 30 років".

// Попросіть користувача ввести своє ім'я та вік

// за допомогою prompt, і викличте функцію gteeting з введеними даними(передавши їх як аргументи). Результат виклику функції виведіть з допомогою alert.

function greeting(person) {
    return "Привіт, мені " + person.age + " років";
}

let name1 = prompt("Введіть ваше ім'я:");
let age = prompt("Введіть ваш вік:");

let user = {
    name1: name1,
    age: age
};

alert(greeting(cloneObject(user)));

// 3.Опціональне. Завдання:
// Реалізувати повне клонування об'єкта.

// Технічні вимоги:
// - Написати функцію для рекурсивного повного клонування об'єкта (без єдиної передачі за посиланням, внутрішня вкладеність властивостей об'єкта може бути досить великою).
// - Функція має успішно копіювати властивості у вигляді об'єктів та масивів на будь-якому рівні вкладеності.
// - У коді не можна використовувати вбудовані механізми клонування, такі як функція Object.assign() або spread.

function cloneObject(obj) {
    if (obj === null || typeof obj !== 'object') {
        return obj;
    }

    let clone = Array.isArray(obj) ? [] : {};

    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            clone[key] = cloneObject(obj[key]);
        }
    }

    return clone;
}

let originalObject = {
    name2: "John",
    age: 30,
    address: {
        city: "New York",
        zip: "10001"
    },
    hobbies: ["reading", "swimming"]
};

let clonedObject = cloneObject(originalObject);
console.log(clonedObject);